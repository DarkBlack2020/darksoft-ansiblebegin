Role Name
=========

A lightweight role to install LAMP

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      become: yes
      gather_facts: yes
      roles:
         - elegantscodes.lamp

License
-------

MIT

Author Information
------------------

You can contact the author via yaser.amini67@gmail.com address.
